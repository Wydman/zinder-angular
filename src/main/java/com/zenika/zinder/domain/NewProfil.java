package com.zenika.zinder.domain;

import java.util.List;

public class NewProfil {

    private String nom;

    private String prenom;

    private String photoUrl;

    private List<String> interets;

    public NewProfil(String nom, String prenom, String photoUrl, List<String> interets) {
        this.nom = nom;
        this.prenom = prenom;
        this.photoUrl = photoUrl;
        this.interets = interets;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public List<String> getInterets() {
        return interets;
    }

    public void setInterets(List<String> interets) {
        this.interets = interets;
    }
}
