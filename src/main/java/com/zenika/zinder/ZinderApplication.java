package com.zenika.zinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ZinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZinderApplication.class, args);
	}

}
