package com.zenika.zinder.controller;

import com.zenika.zinder.domain.*;
import com.zenika.zinder.service.ZinderService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/zinder")
public class ZinderController {

    private ZinderService zinderService;

    public ZinderController(ZinderService zinderService) {
        this.zinderService = zinderService;
    }

    @PostMapping("/profils")
    public Profil createProfil(@RequestBody NewProfil body) {
        return zinderService.saveProfil(body);
    }

    @GetMapping("/profils")
    public Profils getProfils() {
        List<Profil> profils = zinderService.getAllProfils();
        Profils result = new Profils();
        result.setNbProfils(profils.size());
        result.setProfils(profils);
        return result;
    }

    @PostMapping("/profils/{idProfil}/match")
    public Match createProfil(@PathVariable String idProfil, @RequestBody NewMatch body) {
        return zinderService.saveMatch(idProfil, body);
    }

    @GetMapping("/interets")
    public List<Interet> getInterets() {
        return zinderService.getAllInterets();
    }

    @GetMapping("/matchs")
    public List<Match> getMatchs() {
        return zinderService.getAllMatchs();
    }

    @DeleteMapping("/matchs/{idMatch}")
    public void deleteMatch(@PathVariable String idMatch){
        zinderService.deleteMatch(idMatch);
    }
}
